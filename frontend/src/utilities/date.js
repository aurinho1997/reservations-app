/**
 * @param {number} year
 * @param {number} month
 * @returns {number}
 */
export const getDaysInMonth = (year, month) => {
    return new Date(year, month, 0).getDate();
};

/**
 * @param {number} year
 * @param {number} month
 * @param {number} day
 * @returns {number}
 */
export const getDayInWeek = (year, month, day) => {
    const dayInWeek = new Date(year, month, day).getDay();

    return dayInWeek === 0 ? 7 : dayInWeek;
}

/**
 * @param {number} month
 * @returns {string}
 */
export const getMonthName = (month) => {
    const date = new Date(2000, month, 1);
    return date.toLocaleString('default', { month: 'long' });
}

/**
 * @param {Date} date
 * @returns {string}
 */
export const getDateText = date => {
    const year = date.getFullYear();
    const monthName = getMonthName(date.getMonth());
    const hours = date.getHours();
    const day = date.getDate();
    let minutes = date.getMinutes();
    minutes = minutes < 10 ? '00' : minutes;

    return `${year} ${monthName} ${day} - ${hours}:${minutes}`;
};

/**
 *
 * @param {Date} date
 * @returns {boolean}
 */
export const isToday = (date) => {
    const today = new Date()
    return date.getDate() === today.getDate() &&
        date.getMonth() === today.getMonth() &&
        date.getFullYear() === today.getFullYear()
};

/**
 *
 * @param {Date} date
 * @returns {boolean}
 */
export const isPastDay = (date) => {
    const today = new Date(new Date().setHours(0, 0, 0, 0));
    const checkingDate = new Date(new Date(date).setHours(0, 0, 0, 0));

    return checkingDate < today;
};

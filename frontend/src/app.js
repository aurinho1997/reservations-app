import { Fragment } from 'react';

import { useApp } from './talons/App/useApp';
import { getDateText } from './utilities/date';

import './app.scss';

import Calendar from './components/Calendar';
import ReservationList from './components/ReservationList';
import BookReservationForm from './components/BookReservationForm';
import BookReservationSuccess from './components/BookReservationSuccess';
import Spinner from './components/Spinner';
import Backdrop from './components/Backdrop';
import Modal from './components/Modal';
import Error from './components/Error';

import { GET_RESERVATIONS } from './queries/getReservations.gql';

const App = () => {
    const talonProps = useApp({
        queries: {
            getReservationsQuery: GET_RESERVATIONS
        }
    });

    const {
        handleOpenReservationModal,
        handleCloseReservationModal,
        handleBackToReservationList,
        handleSelectReservation,
        handleReservationSuccess,

        isReservationModalOpened,
        selectedReservation,
        bookedReservation,

        reservationListData,
        reservationListError,
        reservationListLoading,
    } = talonProps;

    let modalContent = <Spinner />;

    if (!reservationListLoading && reservationListData) {
        modalContent = <ReservationList
            reservations={reservationListData.reservations}
            onItemButtonClick={handleSelectReservation}
        />;
    }

    if (reservationListError) {
        modalContent = <Error message={reservationListError.message} />
    }

    if (selectedReservation) {
        modalContent = <BookReservationForm
            reservationDate={new Date(selectedReservation.date)}
            onSuccess={handleReservationSuccess} />;
    }

    if (bookedReservation) {
        modalContent = <BookReservationSuccess bookedReservation={bookedReservation}/>
    }

    const modalTitle = selectedReservation ?
        getDateText(new Date(selectedReservation.date)) :
            bookedReservation ? 'Reservation succeeded' : 'Select reservation time';

    return (
        <main className="main-content">
            <h1>Select reservation date</h1>
            <Calendar onItemClick={handleOpenReservationModal} />
            {isReservationModalOpened && (
                <Fragment>
                    <Modal
                        title={modalTitle}
                        canBack={!!selectedReservation}
                        onBack={handleBackToReservationList}
                        onClose={handleCloseReservationModal}
                    >
                        {modalContent}
                    </Modal>
                    <Backdrop onClick={handleCloseReservationModal} />
                </Fragment>
            )}
        </main>
    );
};

export default App;

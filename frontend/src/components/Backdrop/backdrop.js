import React from 'react';
import { func } from 'prop-types';

import './backdrop.scss';

const Backdrop = ({ onClick }) => <div className="backdrop" onClick={onClick} />;

Backdrop.propTypes = {
    onClick: func
};

Backdrop.defaultProps = {
    onClick: () => {}
};

export default Backdrop;

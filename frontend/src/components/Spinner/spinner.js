import React from 'react';

import './spinner.scss';

const Spinner = () => (
    <div className="spinner">
        <div className="spinner__dual-ring" />
    </div>
);

export default Spinner;

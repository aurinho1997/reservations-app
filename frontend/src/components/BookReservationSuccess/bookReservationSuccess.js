import { string, shape } from 'prop-types';

import { getDateText } from '../../utilities/date';

import './bookReservationSuccess.scss';

const BookReservationSuccess = props => {
    const { bookedReservation } = props;

    return (
        <div className="reservation-data">
            <div className="reservation-data__row">
                <span className="reservation-data__row__key">Name:</span>
                <span className="reservation-data__row__value">{bookedReservation.firstName}</span>
            </div>
            <div className="reservation-data__row">
                <span className="reservation-data__row__key">Last name:</span>
                <span className="reservation-data__row__value">{bookedReservation.lastName}</span>
            </div>
            <div className="reservation-data__row">
                <span className="reservation-data__row__key">Reservation date:</span>
                <span className="reservation-data__row__value">{getDateText(new Date(bookedReservation.date))}</span>
            </div>
        </div>
    );
};

BookReservationSuccess.propTypes = {
    bookedReservation: shape({
        firstName: string,
        lastName: string,
        date: string
    }).isRequired
};

export default BookReservationSuccess;

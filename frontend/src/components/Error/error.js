import { string } from 'prop-types';

import './error.scss';

const Error = props => {
    const { message } = props;

    return (
        <div className="error-container">
            <span className="error-message">{message}</span>
        </div>
    )
};

Error.propTypes = {
    message: string
};

Error.defaultProps = {
    message: ''
};

export default Error;

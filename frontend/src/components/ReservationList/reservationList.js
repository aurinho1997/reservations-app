import { func, arrayOf } from 'prop-types';
import reservationType from './reservationType';

import './reservationList.scss';

import ReservationItem from './reservationItem';

const ReservationList = props => {
    const { reservations, onItemButtonClick } = props;

    return (
        <ul className="reservation-list">
            {reservations.map(reservation => {
                return <ReservationItem
                    onButtonClick={onItemButtonClick}
                    key={reservation._id}
                    reservation={reservation}
                />
            })}
        </ul>
    );
};

ReservationList.propTypes = {
    reservations: arrayOf(reservationType).isRequired,
    onItemButtonClick: func
};

ReservationItem.defaultProps = {
    onItemButtonClick: reservation => {}
};

export default ReservationList;

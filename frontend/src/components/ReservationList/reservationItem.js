import { func } from 'prop-types';
import reservationType from './reservationType';

import { getDateText } from '../../utilities/date';

import './reservationItem.scss';

const ReservationItem = props => {
    const { reservation, onButtonClick } = props;

    const isReservationDisabled = () => {
        return reservation.reserved || reservation.expired;
    };

    const reservationButtonText = () => {
        if (reservation.expired) {
            return 'Expired';
        }

        if (reservation.reserved) {
            return 'Reserved';
        }

        return 'Select';
    };

    return (
        <li className="reservation-item">
            <span className="reservation-item__info">
                {getDateText(new Date(reservation.date))}
            </span>
            <div className="reservation-item__actions">
                <div className="reservation-item__action">
                    <button
                        type="button"
                        className="button primary"
                        disabled={isReservationDisabled()}
                        onClick={() => onButtonClick(reservation)}>
                        {reservationButtonText()}
                    </button>
                </div>
            </div>
        </li>
    );
};

ReservationItem.propTypes = {
    reservation: reservationType.isRequired,
    onButtonClick: func
};

ReservationItem.defaultProps = {
    onButtonClick: reservation => {}
};

export default ReservationItem;

import { bool, string, shape } from 'prop-types';

export default shape({
    _id: string,
    date: string,
    empty: bool,
    expired: bool
});

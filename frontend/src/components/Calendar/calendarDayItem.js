import {
    instanceOf,
    bool,
    oneOf,
    func,
    number,
    shape
} from 'prop-types';

import { isToday, isPastDay } from '../../utilities/date';

import './calendarDayItem.scss';

const CalendarDayItem = props => {
    const { allowToClickPastDay, dayData, onClick } = props;

    const additionalClasses = () => {
        let classes = '';

        switch (dayData.statusInMonth) {
            case 'previous':
                classes = classes + 'previous-month ';
                break;
            case 'current':
                classes = classes + 'current-month ';
                break
            case 'next':
                classes = classes + 'next-month '
                break;
            default:
                break;
        }

        if (isToday(dayData.date)) {
            classes = classes + 'today ';
        } else if (isPastDay(dayData.date)) {
            classes = classes + 'past-day ';

            if (!allowToClickPastDay) {
                classes = classes + 'not-allow';
            }
        }

        return classes;
    };

    const handleClick = () => {
        if (!allowToClickPastDay && isPastDay(dayData.date)) return;

        onClick(dayData);
    };

    return (
        <li className={`day-item ${additionalClasses()}`} onClick={handleClick}>
            <span className="day-item__day-text">{dayData.day}</span>
        </li>
    );
};

CalendarDayItem.propTypes = {
    allowToClickPastDay: bool,
    dayData: shape({
        day: number,
        date: instanceOf(Date),
        statusInMonth: oneOf(['previous', 'current', 'next'])
    }).isRequired,
    onClick: func
};

CalendarDayItem.defaultProps = {
    allowToClickPastDay: true,
    onClick: dayData => {}
}

export default CalendarDayItem;

import { bool, func } from 'prop-types';

import { useCalendar } from '../../talons/Calendar/useCalendar';

import './calendar.scss';

import CalendarDayItem from './calendarDayItem';

const Calendar = props => {
    const { onItemClick } = props;
    const talonProps = useCalendar();

    const {
        currentMonthName,
        currentYear,
        dayList,
        handleNextMonth,
        handlePrevMonth,
    } = talonProps;

    return (
        <div className="calendar">
            <div className="calendar__heading">
                <div className="calendar__heading__controls">
                    <button className="button primary" onClick={handlePrevMonth}>❮</button>
                    <h2>{currentMonthName} {currentYear}</h2>
                    <button className="button primary" onClick={handleNextMonth}>❯</button>
                </div>

                <div className="calendar__heading__divider"/>

                <ul className="calendar__heading__week-day-list">
                    <li className="calendar__week-day-item">MON</li>
                    <li className="calendar__week-day-item">TUE</li>
                    <li className="calendar__week-day-item">WEN</li>
                    <li className="calendar__week-day-item">THU</li>
                    <li className="calendar__week-day-item">FRI</li>
                    <li className="calendar__week-day-item">SAT</li>
                    <li className="calendar__week-day-item">SUN</li>
                </ul>
            </div>

            <ul className="calendar__day-list">
                {dayList.map((dayData, index) => {
                    return <CalendarDayItem
                        key={index}
                        onClick={onItemClick}
                        dayData={dayData}
                        {...props}
                    />
                })}
            </ul>
        </div>
    );
};

Calendar.propTypes = {
    allowToClickPastDay: bool,
    onItemClick: func
}

Calendar.defaultProps = {
    allowToClickPastDay: false,
    onItemClick: dayData => {},
};

export default Calendar;

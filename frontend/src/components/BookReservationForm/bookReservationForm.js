import { Fragment } from 'react';
import { instanceOf, func } from 'prop-types';

import { useBookReservationForm } from '../../talons/BookReservationForm/useBookReservationForm';

import './bookReservationForm.scss';

import Error from '../Error';
import Spinner from '../Spinner';

import { BOOK_RESERVATION } from '../../queries/bookReservation.gql';

const BookReservationForm = props => {
    const { reservationDate, onSuccess } = props;

    const talonProps = useBookReservationForm({
        reservationDate,
        mutations: {
            bookReservationMutation: BOOK_RESERVATION
        },
        onSuccess
    });

    const {
        handleBookReservation,
        error,
        loading,
        formErrors,
        firstNameRef,
        lastNameRef
    } = talonProps;

    const content = !loading ? (
        <form className="book-reservation-form" onSubmit={handleBookReservation}>
            <div className="book-reservation-form__controls">
                <div className="book-reservation-form__control">
                    <label>
                        First Name:
                        <input
                            className={formErrors.firstName !== '' ? 'error' : ''}
                            type="text"
                            name="firstName"
                            ref={firstNameRef} />
                    </label>
                    {formErrors.firstName !== '' && (
                        <div className="error-message-container">
                            <span className="error-message">{formErrors.firstName}</span>
                        </div>
                    )}
                </div>
                <div className="book-reservation-form__control">
                    <label>
                        Last Name:
                        <input
                            className={formErrors.lastName !== '' ? 'error' : ''}
                            type="text"
                            name="lastName"
                            ref={lastNameRef} />
                    </label>
                    {formErrors.lastName !== '' && (
                        <div className="error-message-container">
                            <span className="error-message">{formErrors.lastName}</span>
                        </div>
                    )}
                </div>
            </div>

            <div className="book-reservation-form__actions">
                <div className="book-reservation-form__action">
                    <button
                        type="submit"
                        className="button primary">
                        Submit
                    </button>
                </div>
            </div>
        </form>
    ) : <Spinner />;

    return (
        <Fragment>
            {error && <Error message={error.message} />}
            {content}
        </Fragment>
    );
};

BookReservationForm.propTypes = {
    reservationDate: instanceOf(Date).isRequired,
    onSuccess: func
};

BookReservationForm.defaultProps = {
    onSuccess: data => {}
};

export default BookReservationForm;

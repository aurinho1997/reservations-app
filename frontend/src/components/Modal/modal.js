import { useEffect } from 'react';
import { bool, func, string } from 'prop-types';

import './modal.scss';

const Modal = props => {
    const {
        title,
        onClose,
        onBack,
        canBack,
    } = props;

    useEffect(() => {
        const close = event => {
            if (event.key === 'Escape') {
                onClose();
            }
        };

        window.addEventListener('keydown', close);
        return () => window.removeEventListener('keydown', close);
    },[onClose]);

    return (
        <div className="modal">
            <header className="modal__header">
                {canBack && (
                    <button className="back" onClick={onBack}>❮</button>
                )}
                <h1>{title}</h1>
                <button className="close" onClick={onClose} />
            </header>
            <section className="modal__content">
                {props.children}
            </section>
        </div>
    );
};

Modal.propTypes = {
    title: string,
    canBack: bool,
    onBack: func,
    onClose: func
};

Modal.defaultProps = {
    title: 'Modal',
    canBack: false,
    onBack: () => {},
    onClose: () => {}
};

export default Modal;

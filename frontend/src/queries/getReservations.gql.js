import { gql } from '@apollo/client';

export const GET_RESERVATIONS = gql`
    query GetReservations($date: String!) {
        reservations(date: $date) {
            _id
            date
            reserved
            expired
        }
    }
`;

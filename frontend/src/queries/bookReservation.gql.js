import { gql } from '@apollo/client';

export const BOOK_RESERVATION = gql`
    mutation BookReservation(
        $firstName: String!
        $lastName: String!
        $date: String!
    ) {
        bookReservation(reservationInput: {
            firstName: $firstName
            lastName: $lastName
            date: $date
        }) {
            _id
            firstName
            lastName
            date
            createdAt
            updatedAt
        }
    }
`;

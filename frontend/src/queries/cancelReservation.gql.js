import { gql } from '@apollo/client';

export const CANCEL_RESERVATION = gql`
    mutation CancelReservation($reservationId: String!) {
        cancelReservation(reservationId: $reservationId)
    }
`;

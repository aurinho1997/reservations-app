import { useCallback, useRef, useState } from 'react';
import { useMutation } from '@apollo/client';

const isNameValid = (name) => {
    const nameRegex = /^[a-z '-]+$/i;

    return !!name.match(nameRegex);
}

const INITIAL_FORM_ERRORS = {
    firstName: '',
    lastName: '',
    count: 0
}

export const useBookReservationForm = props => {
    const {
        reservationDate,
        mutations: { bookReservationMutation },
        onSuccess
    } = props;

    const [formErrors, setFormErrors] = useState(INITIAL_FORM_ERRORS);
    const firstNameRef = useRef(null);
    const lastNameRef = useRef(null);

    const [bookReservation, {
        loading,
        error,
        data
    }] = useMutation(bookReservationMutation);

    const validateForm = useCallback(() => {
        setFormErrors(INITIAL_FORM_ERRORS);

        const firstName = firstNameRef.current.value;
        const lastName = lastNameRef.current.value;
        const errors = { ...INITIAL_FORM_ERRORS };

        if (!isNameValid(lastName)) {
            errors.lastName = 'Incorrect input value';
            errors.count++;
            lastNameRef.current.focus();
        }

        if (!isNameValid(firstName)) {
            errors.firstName = 'Incorrect input value';
            errors.count++;
            firstNameRef.current.focus();
        }

        if (errors.count) {
            setFormErrors(errors);
            return false;
        }

        return true;
    }, [setFormErrors]);

    const handleBookReservation = useCallback(event => {
        event.preventDefault();

        if (!validateForm()) return;

        const firstName = firstNameRef.current.value;
        const lastName = lastNameRef.current.value;

        bookReservation({
            variables: {
                firstName: firstName,
                lastName: lastName,
                date: reservationDate
            }
        }).then((res) => {
            onSuccess(res.data.bookReservation);
        }).catch(() => {});
    }, [bookReservation, onSuccess, reservationDate, validateForm]);

    return {
        handleBookReservation,
        loading,
        error,
        data,
        formErrors,
        firstNameRef,
        lastNameRef
    };
};

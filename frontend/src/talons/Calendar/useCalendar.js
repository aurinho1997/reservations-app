import { useCallback, useEffect, useState } from 'react'

import { getDaysInMonth, getDayInWeek, getMonthName } from '../../utilities/date';

/**
 * @typedef {'previous' | 'current' | 'next'} StatusInMonth
 */
/**
 * @typedef {{ date: Date, day: number, statusInMonth: StatusInMonth }} DayData
 */

/**
 * @param {number} year
 * @param {number} month
 * @param {number} day
 * @param {StatusInMonth} statusInMonth
 * @returns {DayData}
 */
const createDayData = (year, month, day, statusInMonth) => {
    return {
        day,
        date: new Date(year, month, day),
        statusInMonth
    };
};

/**
 * @param {Date} currentDate
 * @returns {DayData[]}
 */
const getCurrentMonthDayList = currentDate => {
    const year = currentDate.getFullYear();
    const month = currentDate.getMonth() + 1;

    const daysInMonth = getDaysInMonth(year, month);
    const daysInPrevMonth = getDaysInMonth(year, month - 1);
    const firstMonthDayInWeek = getDayInWeek(year,  month - 1, 1);//?
    const lastMonthDayInWeek = getDayInWeek(year,  month - 1, daysInMonth);//?

    const missingDaysBeforeMonth = firstMonthDayInWeek - 1;
    const missingDaysAfterMonth = 7 - lastMonthDayInWeek;

    const totalDaysInList = daysInMonth + missingDaysBeforeMonth + missingDaysAfterMonth;

    return Array.from(
        { length: totalDaysInList },
        (_, i) => {
            let day;
            let dayIndex = i + 1;

            if (dayIndex <= missingDaysBeforeMonth) {
                day = daysInPrevMonth - (missingDaysBeforeMonth - dayIndex);

                return createDayData(year, month - 2, day, 'previous');
            }

            dayIndex = i + 1 - missingDaysBeforeMonth;
            day = dayIndex;

            if (dayIndex > daysInMonth) {
                day = dayIndex - daysInMonth;

                return createDayData(year, month, day, 'next');
            }

            return createDayData(year, month - 1, day, 'current');
    });
};

export const useCalendar = () => {
    const [currentDate, setCurrentDate] = useState(new Date());
    const [dayList, setDayList] = useState([]);

    useEffect(() => {
        setDayList(getCurrentMonthDayList(currentDate));
    }, [currentDate]);

    const handleNextMonth = useCallback(() => {
        const currentMonth = currentDate.getMonth();
        const nextMonthDate = new Date(currentDate.setMonth(currentMonth + 1));

        setCurrentDate(nextMonthDate);
    }, [currentDate, setCurrentDate]);

    const handlePrevMonth = useCallback(() => {
        const currentMonth = currentDate.getMonth();
        const prevMonthDate = new Date(currentDate.setMonth(currentMonth - 1));

        setCurrentDate(prevMonthDate);
    }, [currentDate, setCurrentDate]);

    const currentMonthName = getMonthName(currentDate.getMonth());
    const currentYear = currentDate.getFullYear();

    return {
        currentMonthName,
        currentYear,
        dayList,
        handleNextMonth,
        handlePrevMonth
    };
};

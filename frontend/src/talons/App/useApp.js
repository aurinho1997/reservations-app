import { useCallback, useState } from 'react';
import { useLazyQuery } from '@apollo/client';

export const useApp = props => {
    const {
        queries: { getReservationsQuery }
    } = props;

    const [isReservationModalOpened, setIsReservationModalOpened] = useState(false);
    const [selectedReservation, setSelectedReservation] = useState(null);
    const [bookedReservation, setBookedReservation] = useState(null);

    const [runGetReservations, {
        loading: reservationListLoading,
        error: reservationListError,
        data: reservationListData
    }] = useLazyQuery(getReservationsQuery);

    const handleOpenReservationModal = useCallback(dayData => {
        runGetReservations({
            variables: { date: dayData.date },
        });
        setIsReservationModalOpened(true);
    }, [runGetReservations]);

    const handleBackToReservationList = useCallback(() => {
        setSelectedReservation(null);
    }, [setSelectedReservation]);

    const handleCloseReservationModal = useCallback(() => {
        setIsReservationModalOpened(false);
        setBookedReservation(null);
        setSelectedReservation(null);
    }, [
        setBookedReservation,
        setIsReservationModalOpened,
        setSelectedReservation
    ]);

    const handleSelectReservation = useCallback(reservationData => {
        setSelectedReservation(reservationData);
    }, [setSelectedReservation]);

    const handleReservationSuccess = useCallback(bookedReservationData => {
        setSelectedReservation(null);
        setBookedReservation(bookedReservationData);
    }, [setBookedReservation, setSelectedReservation]);

    return {
        handleOpenReservationModal,
        handleCloseReservationModal,
        handleBackToReservationList,
        handleSelectReservation,
        handleReservationSuccess,

        isReservationModalOpened,
        bookedReservation,
        selectedReservation,

        reservationListData,
        reservationListError,
        reservationListLoading,
    };
};

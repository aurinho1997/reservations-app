## Steps to start app:

1. In `/root/` directory run `npm install`
2. In `/root/frontend/` directory run `npm install`
3. In `/root/` directory run `node app.js`
4. In `/root/frontend/` directory run `npm start`

### Preview:
![preview2.png](preview2.png)

![preview.png](preview.png)
